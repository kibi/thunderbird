# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this file,
# You can obtain one at http://mozilla.org/MPL/2.0/.

menubar        =       barra de menú
scrollbar      =       barra de desprazamento
grip           =       tirador
alert          =       alerta
menupopup      =       menú emerxente
document       =       documento
pane           =       panel
dialog         =       diálogo
separator      =       separador
toolbar        =       barra de ferramentas
statusbar      =       barra de estado
table          =       táboa
columnheader   =       cabeceira da columna
rowheader      =       cabeceira da fila
column         =       columna
row            =       fila
cell           =       cela
link           =       ligazón
list           =       lista
listitem       =       elemento da lista
outline        =       esquema
outlineitem    =       elemento do esquema
pagetab        =       lapela da páxina
propertypage   =       páxina de propiedades
graphic        =       gráfico
pushbutton     =       botón
checkbutton    =       caixa de verificación
radiobutton    =       botón de opción
combobox       =       caixa de combinación
progressbar    =       barra de progreso
slider         =       control desprazábel
spinbutton     =       botón xiratorio
diagram        =       diagrama
animation      =       animación
equation       =       ecuación
buttonmenu     =       botón de menú
whitespace     =       espazo en branco
pagetablist    =       lista de lapelas da páxina
canvas         =       lenzo
checkmenuitem  =       elemento de menú de verificación
label          =       etiqueta
passwordtext   =       texto do contrasinal
radiomenuitem  =       elemento do menú de opcións
textcontainer  =       contedor de texto
togglebutton   =       botón de estado
treetable      =       táboa en árbore
header         =       cabeceira
footer         =       pé de páxina
paragraph      =       parágrafo
entry          =       entrada
caption        =       lenda
heading        =       título
section        =       sección
form           =       formulario
comboboxlist   =       lista da caixa de combinación
comboboxoption =       opción da caixa de combinación
imagemap       =       mapa da imaxe
listboxoption  =       opción da caixa de lista
listbox        =       caixa de lista
flatequation   =       ecuación
gridcell       =       grade de celas
note           =       nota
figure         =       figura
definitionlist =       Lista de definición
term           =       termo
definition     =       definición

# More sophisticated roles which are not actual numeric roles
textarea       =       área de texto

# More sophisticated object descriptions
headingLevel   =       nivel do título %S

# more sophisticated list announcement
listStart      =       Primeiro elemento
listEnd        =       Último elemento
listItemCount  =       %S elementos

# Invoked actions
jumpAction     =      saltado
pressAction    =      premido
checkAction    =      marcado
uncheckAction  =      desmarcado
selectAction   =      seleccionado
openAction     =      aberto
closeAction    =      pechado
switchAction   =      cambiado
clickAction    =      premido
collapseAction =      contraído
expandAction   =      expandido
activateAction =      activado
cycleAction    =      ciclo

# Tab states
tabLoading     =      cargando
tabLoaded      =      cargada
tabNew         =      nova lapela
tabLoadStopped =      detívose a carga
tabReload      =      recargando

# Object states
stateChecked     =    comprobado
stateNotChecked  =    sen comprobar
stateExpanded    =    expandido
stateCollapsed   =    contraído
stateUnavailable =    non dispoñíbel
stateRequired    =    requirido
stateTraversed   =    visitado

# App modes
editingMode    =      edición
navigationMode =      navegación

alertAbbr          =       alert
animationAbbr      =       animation

# Landmark announcements
banner         =       banner

base           =       base

baseAbbr           = base
buttonmenuAbbr     =       button menu
canvasAbbr         =       canvas
captionAbbr        =       caption
cellAbbr           =       cell
cellInfoAbbr = c%Sr%S
checkbuttonAbbr    =       check button
checkmenuitemAbbr  =       check menu item
close-fence    =       closing fence
close-fenceAbbr    = close
columnAbbr         =       column

# table or grid cell information
columnInfo = Column %S
columnheaderAbbr   =       column header
comboboxAbbr       =       combo box
comboboxlistAbbr   =       combo box list
comboboxoptionAbbr =       combo box option
complementary  =       complementary
contentinfo    =       content info
definitionAbbr     =       definition
definitionlistAbbr =       definition list
denominator    =       denominator
denominatorAbbr    = den
diagramAbbr        =       diagram
dialogAbbr         =       dialog
documentAbbr       =       document
entryAbbr          =       entry
equationAbbr       =       equation
figureAbbr         =       fig
flatequationAbbr   =       flat equation
footerAbbr         =       footer
formAbbr           =       form
graphicAbbr        =       graphic
gridcellAbbr       =       gridcell
gripAbbr           =       grip
headerAbbr         =       header
headingAbbr        =       heading

# Live regions
# 'hidden' will be spoken when something disappears in a live region.
hidden         =      hidden
imagemapAbbr       =       imgmap
labelAbbr          =       label
linkAbbr           =       lnk
listAbbr           =       list
# LOCALIZATION NOTE (listItemsCount): Semi-colon list of plural forms.
# See: http://developer.mozilla.org/en/docs/Localization_and_Plurals
listItemsCount =       1 item;#1 items
listboxAbbr        =       list box
listboxoptionAbbr  =       option
listitemAbbr       =       list item
main           =       main
mathmlcell               = cell
mathmlcellAbbr               = cell
mathmlenclosed           = enclosed

mathmlenclosedAbbr           = enclosed
mathmlfraction           = fraction
mathmlfractionAbbr           = frac
mathmlfractionwithoutbar = fraction without bar
mathmlfractionwithoutbarAbbr = frac no bar
mathmlroot               = root
mathmlrootAbbr               = root
mathmlscripted           = scripted
mathmlscriptedAbbr           = scripted
mathmlsquareroot         = square root
mathmlsquarerootAbbr         = sqrt

mathmltable              = math table
mathmltableAbbr              = tbl

# Shortened role names for braille
menubarAbbr        =       menu bar
menupopupAbbr      =       menu popup
navigation     =       navigation
notation-actuarial          = actuarial
notation-actuarialAbbr          = act
notation-bottom             = bottom
notation-bottomAbbr             = bot
notation-box                = box
notation-boxAbbr                = box
notation-circle             = circle
notation-circleAbbr             = circ
notation-downdiagonalstrike = down diagonal strike
notation-downdiagonalstrikeAbbr = dwndiagstrike
notation-horizontalstrike   = horizontal strike
notation-horizontalstrikeAbbr   = hstrike
notation-left               = left
notation-leftAbbr               = lft

# MathML menclose notations.
# See developer.mozilla.org/docs/Web/MathML/Element/menclose#attr-notation
notation-longdiv            = long division

notation-longdivAbbr            = longdiv
notation-madruwb            = madruwb
notation-madruwbAbbr            = madruwb
notation-phasorangle        = phasor angle
notation-phasorangleAbbr        = phasang
notation-radical            = radical
notation-radicalAbbr            = rad
notation-right              = right
notation-rightAbbr              = rght
notation-roundedbox         = rounded box
notation-roundedboxAbbr         = rndbox
notation-top                = top
notation-topAbbr                = top
notation-updiagonalarrow    = up diagonal arrow
notation-updiagonalarrowAbbr    = updiagarrow
notation-updiagonalstrike   = up diagonal strike
notation-updiagonalstrikeAbbr   = updiagstrike
notation-verticalstrike     = vertical strike
notation-verticalstrikeAbbr     = vstrike
noteAbbr           =       note
numerator      =       numerator
numeratorAbbr      = num

# LOCALIZATION NOTE: # %1$S is the position of the item n the set.
# %2$S is the total number of such items in the set.
# An expanded example would read "2 of 5".
objItemOfN      =       %1$S of %2$S
offAction      =      off
onAction       =      on
open-fence     =       opening fence
open-fenceAbbr     = open
outlineAbbr        =       outline
outlineitemAbbr    =       outline item
overscript     =       overscript
overscriptAbbr     = over
pagetabAbbr        =       tab
pagetablistAbbr    =       tab list
paneAbbr           =       pane
paragraphAbbr      =       paragraph
passwordtextAbbr   =       passwdtxt
presubscript   =       presubscript
presubscriptAbbr   = presub
presuperscript =       presuperscript
presuperscriptAbbr = presup
progressbarAbbr    =       progress bar
propertypageAbbr   =       property page
pushbuttonAbbr     =       btn
quicknav_Anchor      = Anchors
quicknav_Button      = Buttons
quicknav_Checkbox    = Check boxes
quicknav_Combobox    = Combo boxes
quicknav_Entry       = Entries
quicknav_FormElement = Form elements
quicknav_Graphic     = Images
quicknav_Heading     = Headings
quicknav_Landmark    = Landmarks
quicknav_Link        = Links
quicknav_List        = Lists
quicknav_ListItem    = List items
quicknav_PageTab     = Page tabs
quicknav_RadioButton = Radio buttons
quicknav_Separator   = Separators

# Quick navigation modes
quicknav_Simple      = Default
quicknav_Table       = Tables
radiobuttonAbbr    =       radio button
radiomenuitemAbbr  =       radio menu item
root-index     =       root index
root-indexAbbr     = index
rowAbbr            =       row
rowInfo = Row %S
rowheaderAbbr      =       row header
screenReaderStarted = Screen reader started
screenReaderStopped = Screen reader stopped
scrollbarAbbr      =       scroll bar
search         =       search
sectionAbbr        =       section
separatorAbbr      =       separator
sliderAbbr         =       slider
spansColumns = spans %S columns
spansRows = spans %S rows
spinbuttonAbbr     =       spin button

stateCheckedAbbr = (x)
stateHasPopup    =    has pop up
stateOff         =    off
stateOn          =    on
statePressed     =    pressed
statePressedAbbr = (x)
stateReadonly    =    readonly
stateSelected    =    selected
stateUncheckedAbbr = ( )
stateUnpressedAbbr = ( )
statusbarAbbr      =       status bar
subscript      =       subscript
subscriptAbbr      = sub
superscript    =       superscript
superscriptAbbr    = sup
switch         =       switch
tableAbbr          =       tbl

# LOCALIZATION NOTE (tblColumnInfo): Semi-colon list of plural forms.
# Number of columns within the table.
# See: http://developer.mozilla.org/en/docs/Localization_and_Plurals
tblColumnInfo = with 1 column;with #1 columns

# LOCALIZATION NOTE (tblColumnInfoAbbr): Semi-colon list of plural forms.
# Number of columns within the table.
# See: http://developer.mozilla.org/en/docs/Localization_and_Plurals
tblColumnInfoAbbr = #1c;#1c
# LOCALIZATION NOTE (tblRowInfo): Semi-colon list of plural forms.
# Number of rows within the table or grid.
# See: http://developer.mozilla.org/en/docs/Localization_and_Plurals
tblRowInfo = and 1 row;and #1 rows
# LOCALIZATION NOTE (tblRowInfoAbbr): Semi-colon list of plural forms.
# Number of rows within the table or grid.
# See: http://developer.mozilla.org/en/docs/Localization_and_Plurals
tblRowInfoAbbr = #1r;#1r
termAbbr           =       term

# Text input types
textInputType_date   =       date
textInputType_email  =       e-mail
textInputType_search =       search
textInputType_tel    =       telephone
textInputType_url    =       URL
textareaAbbr       =       txtarea
textcontainerAbbr  =       text container
togglebuttonAbbr   =       toggle button
toolbarAbbr        =       toolbar
treetableAbbr      =       tree table
underscript    =       underscript
underscriptAbbr    = under
unselectAction =      unselected
whitespaceAbbr     =       white space
